import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export const getAllProduct = async (req, res) => {
  try {
    const data = await prisma.products.findMany({
      where: {
        remove: false,
      },
    });
    data.length === 0
      ? res.status(404).json({
          statusCode: 404,
          message: "No products found!",
          content: null,
        })
      : res.status(200).json({
          statusCode: 200,
          message: "Successfully!",
          content: data,
        });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      statusCode: 500,
      message: "Internal server error!",
      content: null,
    });
  }
};

export const getProductById = async (req, res) => {
  try {
    const { id } = req.params;
    const data = await prisma.products.findFirst({
      where: {
        id: Number(id),
        remove: false,
      },
    });
    data
      ? res.status(200).json({
          statusCode: 200,
          message: "Successfully!",
          content: data,
        })
      : res.status(404).json({
          statusCode: 404,
          message: "No products found!",
          content: null,
        });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      statusCode: 500,
      message: "Internal server error!",
      content: null,
    });
  }
};

export const updateProduct = async (req, res) => {
  try {
    const { image, name, description, price, color } = req.body;
    const { id } = req.params;
    const checkID = await prisma.products.findFirst({
      where: {
        id: Number(id),
        remove: false,
      },
    });
    checkID
      ? await prisma.products
          .update({
            where: {
              id: Number(id),
            },
            data: { image, name, description, price, color },
          })
          .then(
            res.status(200).json({
              statusCode: 200,
              message: "Update successfully!",
              content: null,
            })
          )
      : res.status(404).json({
          statusCode: 404,
          message: "No products found!",
          content: null,
        });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      statusCode: 500,
      message: "Internal server error!",
      content: null,
    });
  }
};

export const deleteProductById = async (req, res) => {
  try {
    const { id } = req.params;
    const checkID = await prisma.products.findFirst({
      where: {
        id: Number(id),
        remove: false,
      },
    });

    checkID
      ? await prisma.products
          .update({
            where: {
              id: Number(id),
              remove: false,
            },
            data: {
              remove: true,
            },
          })
          .then(
            res.status(200).json({
              statusCode: 200,
              message: "Delete successfully!",
              content: null,
            })
          )
      : res.status(404).json({
          statusCode: 404,
          message: "No products found!",
          content: null,
        });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      statusCode: 500,
      message: "Internal server error!",
      content: null,
    });
  }
};

export const createProduct = async (req, res) => {
  try {
    const { image, name, description, price, color } = req.body;
    const newProduct = await prisma.products.create({
      data: { image, name, description, price, color }
    });
    res.status(201).json({
      statusCode: 201,
      message: "Created successfully!",
      content: newProduct,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      statusCode: 500,
      message: "Internal server error!",
      content: null,
    });
  }
};
