import express from "express";
import { createProduct, deleteProductById, getAllProduct, getProductById, updateProduct } from "../controllers/productsController.js";

const productsRoute = express.Router();

productsRoute.get("/products", getAllProduct); 
productsRoute.get("/products/:id", getProductById); 

productsRoute.post("/products", createProduct); 
productsRoute.put("/products/:id",updateProduct ); 
productsRoute.delete("/products/:id",deleteProductById)

export default productsRoute;
