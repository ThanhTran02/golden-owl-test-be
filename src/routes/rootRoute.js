import express from "express";
import productsRoute from "./productsRoute.js";

const rootRoute = express.Router();
rootRoute.use("/api/v1", productsRoute);

export default rootRoute;
